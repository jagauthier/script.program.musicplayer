import xbmcaddon, xbmcgui, xbmc
from threading import Thread
import time
import dbus
import dbus.service
import random
import textwrap

__addon__     = xbmcaddon.Addon()
__addonpath__ = __addon__.getAddonInfo('path').decode("utf-8")


#global used to tell the worker thread the status of the window
__windowopen__   = True

#capture a couple of actions to close the window
ACTION_PREVIOUS_MENU = 10
ACTION_BACK = 92

#control
BACK_BUTTON  = 1201
TITLE_LABEL  = 1202

PLAY_BUTTON  = 1203
PAUSE_BUTTON = 1204
PREV_BUTTON  = 1205
NEXT_BUTTON  = 1206

PROGRESS_BAR = 1207
PROGRESS_BTN = 1208

POS_LABEL    = 1209
TR_LABEL     = 1210

BG_IMAGE     = 1213

LTXT_BOX     = 1217

L_LIST       = 1218

POPUP_BGS = 6
POPUP_TIME = 5  # seconds


SERVICE_NAME = "org.bluez"
AGENT_IFACE = SERVICE_NAME + '.Agent1'
ADAPTER_IFACE = SERVICE_NAME + ".Adapter1"
DEVICE_IFACE = SERVICE_NAME + ".Device1"
PLAYER_IFACE = SERVICE_NAME + '.MediaPlayer1'
TRANSPORT_IFACE = SERVICE_NAME + '.MediaTransport1'


title="Music Player"

def log(logline):
    print "MUSICXML: " + logline
    
def set_kodi_prop(property, value):
#    log ("Setting: '{}', '{}'".format(property, value))
    strvalue=str(value)
    xbmcgui.Window(10000).setProperty(property, strvalue)

def get_kodi_prop(property):
    value=xbmcgui.Window(10000).getProperty(property)
#    log("Loading: '" + property + "', '" + value[:30] + "'")
    return value

def converttime(ms):
    s=ms/1000 
    m,s=divmod(s,60) 
    h,m=divmod(m,60) 
    d,h=divmod(h,24) 
    return d,h,m,s 

def mstotime(d,h,m,s):
    position=""
    if d>0:
        if d<10:
            position+="0"
        position+=str(d)+":"
        
    if h>0:
        if h<10:
            position+="0"
        position+=str(h)+":"

    if m<10:
        position+="0"
    position+=str(m)+":"
                
    if s<10:
        position+="0"
    position+=str(s)
    return position

def wrap_text(text, len):
    txt_arr=textwrap.wrap(text,len)
    new_txt=""
    for t in txt_arr:
        new_txt=new_txt+t+"[CR]"
    return new_txt

# Gets the next time stamp in lyrics scroll
def get_next_ts(lyric_lines,ts):
	bn=0
	for line in lyric_lines:
		if bn==1:
			return line['timestamp']
		if line['timestamp']==ts:
			bn=1

def update_display():
    d=0
    h=0
    m=0
    s=0
    pos_ms =0
    duration_ms=0
    tr_ms=0
        
    lyrics=get_kodi_prop("current_song_txt")
    if lyrics!=musicplayer.lyrics:
        # lyrics have changed
        musicplayer.lyrics=lyrics

        try:
            musicplayer.lyrics_list=eval(lyrics)            
            musicplayer.lrc=True
            musicplayer.l_list.setVisible(True)
            musicplayer.l_list.setEnabled(True)        
            musicplayer.ltxt_box.setVisible(False)
            musicplayer.ltxt_box.setEnabled(False)
            musicplayer.l_list.reset()
            for line in musicplayer.lyrics_list:
                wrapped=wrap_text(line['text'],35)
                wrap_array=wrapped.split("[CR]")
                if wrap_array[0]:
                    musicplayer.l_list.addItem(wrap_array[0])
                    if wrap_array[1]:
                        musicplayer.l_list.getListItem(musicplayer.l_list.size()-1).setLabel2(wrap_array[1])
                else:
                    musicplayer.l_list.addItem(" ")

        except:
            
            musicplayer.lrc=False
            musicplayer.ltxt_box.setVisible(True)
            musicplayer.ltxt_box.setEnabled(True)
            musicplayer.l_list.setVisible(False)
            musicplayer.l_list.setEnabled(False)

        if lyrics=="":
            musicplayer.l_list.setVisible(False)
            musicplayer.l_list.setEnabled(False)            
            musicplayer.ltxt_box.setVisible(False)
            musicplayer.ltxt_box.setEnabled(False)
        
    #format the strings in case they're wicked long
        
    set_kodi_prop("player_artist_disp", wrap_text(get_kodi_prop("player_artist"), 40))
    set_kodi_prop("player_album_disp", wrap_text(get_kodi_prop("player_album"), 40))
    set_kodi_prop("player_title_disp", wrap_text(get_kodi_prop("player_title"), 40))
        
    if musicplayer.player:
        pos_ms=int(musicplayer.player.Get(PLAYER_IFACE, "Position", dbus_interface="org.freedesktop.DBus.Properties"))
        duration_ms=int(get_kodi_prop("player_duration"))
        tr_ms=duration_ms-pos_ms
        
        # we get some weird readings. If we get them let's just put
        # zeros in the UI. Once you hit pause/play it works.
        # we could try and "fix" that here...
        if tr_ms<0 or pos_ms>duration_ms:
            tr_ms=0
            pos_ms=0
            
        d,h,m,s=converttime(pos_ms)
        musicplayer.pos_label.setLabel(mstotime(d,h,m,s))
        d,h,m,s=converttime(tr_ms)
        musicplayer.tr_label.setLabel("-"+mstotime(d,h,m,s))
        if pos_ms and duration_ms:
            musicplayer.progress_bar.setPercent((pos_ms*100)/duration_ms)
        if musicplayer.lrc==True:
            lyr_lines=len(musicplayer.lyrics_list)
            current_line=0

            this_line=0
            for line in musicplayer.lyrics_list:
                line_time=int(line['timestamp'])
                next_ts=get_next_ts(musicplayer.lyrics_list,line['timestamp'])
                if next_ts and pos_ms>(next_ts-250):
                    stat="old"
                elif pos_ms<line_time:
                    stat="future"
                elif (pos_ms>line_time-250) and (line_time-250>0):
                    stat="next"
                    this_line=current_line
                current_line=current_line+1                
                
#                log ("TRACK: " + stat + " : " + str(pos_ms) + "/" + str(line_time) + " " + str(this_line) +"  " + line['text'] + "/" + musicplayer.l_list.getListItem(this_line).getLabel())
#                log("SIZE: " +str(len(musicplayer.lyrics_list)) + "/" + str(musicplayer.l_list.size()))
            if this_line:
                musicplayer.l_list.selectItem(this_line)
                
        
        # This is actually for simulation purposes
    else:
#        global pos_ms
        duration_ms=(3*60000)+46000
        pos_ms=pos_ms+1000
        tr_ms=duration_ms-pos_ms
        d,h,m,s=converttime(pos_ms)
        musicplayer.pos_label.setLabel(mstotime(d,h,m,s))
        d,h,m,s=converttime(tr_ms)
        musicplayer.tr_label.setLabel("-"+mstotime(d,h,m,s))
        musicplayer.progress_bar.setPercent((pos_ms*100)/duration_ms)
            
        if musicplayer.lrc==True:
            lyr_lines=len(musicplayer.lyrics_list)
            current_line=0
            this_line=0

            for line in musicplayer.lyrics_list:
                line_time=int(line['timestamp'])
                current_line=current_line+1
                next_ts=get_next_ts(musicplayer.lyrics_list,line['timestamp'])
                    
                if next_ts and pos_ms>(next_ts-250):
                    pass
                elif pos_ms<line_time:
                    pass
                elif pos_ms>line_time-250:
                    this_line=current_line
                    break
                        
            if this_line:
                musicplayer.l_list.selectItem(this_line)
        
def updateWindow():

    # I put this here because sometimes the thread starts before the onInit function
    # is complete. When that happens, it throws an exception.
    # We can assume that once this is not None, it's done enough

    while musicplayer.title_label==None:
        time.sleep(.25)
        
    #this is the worker thread that updates the window information every w seconds
    #this strange looping exists because I didn't want to sleep the thread for very long
    #as time.sleep() keeps user input from being acted upon

    if __full_interface__==True:
        while __windowopen__ and (not xbmc.abortRequested):
            update_display()
            # give us a break
            time.sleep(1)
    else:
        while musicplayer.counter < POPUP_TIME:
#            log("Counter: " +str(musicplayer.counter))
            musicplayer.counter=musicplayer.counter+1
            time.sleep(1)
        set_kodi_prop("music_toaster", "")
        mpdialog.leave()
                                                                                                                                                
class musicplayer(xbmcgui.WindowXMLDialog):
    
    button_back=None
    button_play=None
    button_pause=None
    button_prev=None
    button_next=None
    title_label=None    
    progress_bar=None
    pos_label=None
    tr_label=None
    player=None
    bg_image=None
    counter=1

    ltxt_box=None
    l_label=None
    l_list=None
    
    lyrics=None
    lyrics_list=None
    lrc=False
    
    def leave(self):
        log("ENDING.")
        set_kodi_prop("media_player_active", "")        
        __windowopen__ = False        
        self.close()
    
    def __init__(self,strXMLname, strFallbackPath, strDefaultName, forceFallback):
        log("Initializing...")

    def onInit(self):

        musicplayer.button_prev=self.getControl(PREV_BUTTON)
        musicplayer.button_next=self.getControl(NEXT_BUTTON)
      
        if __full_interface__==False:
            musicplayer.bg_image=self.getControl(BG_IMAGE)
            musicplayer.bg_image.setImage("popupbg" + str(random.randint(1, POPUP_BGS)) + ".jpg")
            
        else:
            musicplayer.button_back=self.getControl(BACK_BUTTON)
            musicplayer.button_play=self.getControl(PLAY_BUTTON)
            musicplayer.button_pause=self.getControl(PAUSE_BUTTON)
            musicplayer.progress_bar=self.getControl(PROGRESS_BAR)
            musicplayer.pos_label=self.getControl(POS_LABEL)
            musicplayer.tr_label=self.getControl(TR_LABEL)
            musicplayer.ltxt_box=self.getControl(LTXT_BOX)
            musicplayer.l_list=self.getControl(L_LIST)

            musicplayer.ltxt_box.setVisible(False)
            musicplayer.ltxt_box.setEnabled(False)
            musicplayer.l_list.setVisible(False)
            musicplayer.l_list.setEnabled(False)            

            status=get_kodi_prop("player_status")
            if status=="playing":
                musicplayer.button_pause.setVisible(True)
                musicplayer.button_play.setVisible(False)
            else:
                musicplayer.button_pause.setVisible(False)
                musicplayer.button_play.setVisible(True)            

        # This is loaded last, because we use it in the main looping thread
        # to make sure this function has completed initialization
        musicplayer.title_label=self.getControl(TITLE_LABEL)
        musicplayer.title_label.setLabel(title)
        
        if get_kodi_prop("connected_device")=="":
            pass
#            xbmcgui.Dialog().ok("Music Player", "No device connected")
#            log("ENDING.")
#            __windowopen__ = False
#            set_kodi_prop("media_player_active", "")
#            self.close()
            
        # do dbus connectivity
        self.bus = dbus.SystemBus()
        self.manager = dbus.Interface(self.bus.get_object("org.bluez", "/"), "org.freedesktop.DBus.ObjectManager")
        objects = self.manager.GetManagedObjects()

        set_kodi_prop("media_player_active", "1")
            
        for path, interfaces in objects.iteritems():
            if ADAPTER_IFACE in interfaces:
                self.adapter=self.bus.get_object("org.bluez", path)
                self.adapter_manager=dbus.Interface(self.adapter, 'org.freedesktop.DBus.Properties')
                
            if PLAYER_IFACE in interfaces:
                player_path = path
                musicplayer.player = self.bus.get_object("org.bluez", player_path)

        if musicplayer.player==None:
            pass
#            xbmcgui.Dialog().ok("Music Player", "Can't find music player interface.")
#            log("ENDING.")
#            __windowopen__ = False
#            set_kodi_prop("media_player_active", "")
#            self.close()
            
    def onAction(self, action):
        global __windowopen__
        
        if action == ACTION_PREVIOUS_MENU:
            log("ENDING.")
            __windowopen__ = False
            set_kodi_prop("media_player_active", "")
            self.close()

    def onClick(self, controlID):
        musicplayer.counter=1
        if controlID == BACK_BUTTON:
            log("ENDING.")            
            __windowopen__ = False
            self.close()

        # don't do anything if no BT connection
        bt_device=get_kodi_prop("connected_device")
        if bt_device!="":
            if controlID == PLAY_BUTTON:
                musicplayer.player.Play(dbus_interface=PLAYER_IFACE)
                musicplayer.button_pause.setVisible(True)
                musicplayer.button_play.setVisible(False)
                
            if controlID == PAUSE_BUTTON:
                musicplayer.player.Pause(dbus_interface=PLAYER_IFACE)
                musicplayer.button_pause.setVisible(False)
                musicplayer.button_play.setVisible(True)
                
            if controlID == NEXT_BUTTON:
                musicplayer.player.Next(dbus_interface=PLAYER_IFACE)
                if __full_interface__==True:
                    musicplayer.button_pause.setVisible(False)
                    musicplayer.button_play.setVisible(True)
                
            if controlID == PREV_BUTTON:
                musicplayer.counter=0                
                musicplayer.player.Previous(dbus_interface=PLAYER_IFACE)
                if __full_interface__==True:
                    musicplayer.button_pause.setVisible(False)
                    musicplayer.button_play.setVisible(True)
            
    def onFocus(self, controlID):
        pass
    
    def onControl(self, controlID):
        pass

if  __name__ == '__main__':
    
    if get_kodi_prop("music_toaster")=="":
        log("Loading full dialog");
        __full_interface__=True
        mpdialog = musicplayer("musicplayer.xml", __addonpath__, 'default', '720p')        
    else:
        log("Loading toaster dialog");        
        __full_interface__=False        
        mpdialog = musicplayer("musictoaster.xml", __addonpath__, 'default', '720p')        
    
    t1 = Thread( target=updateWindow)
    t1.setDaemon( True )
    t1.start()
    mpdialog.doModal()
    del mpdialog        
    
    




    




